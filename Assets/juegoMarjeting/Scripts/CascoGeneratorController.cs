﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CascoGeneratorController : MonoBehaviour {

	public GameObject cascoPrefab;
	public float generatorTimer=6f;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void CreateCasco(){
		Instantiate (cascoPrefab, transform.position, Quaternion.identity);
	}

	public void StopGenerator(bool clean=false){
		CancelInvoke ("CreateCasco");
		if (clean) {
			object[] allCasco = GameObject.FindGameObjectsWithTag ("Casco");
			foreach (GameObject casco in allCasco)
				Destroy (casco);
		}
	}

	public void StartGenerator(){
		InvokeRepeating ("CreateCasco", 0f, generatorTimer);
	}
}
