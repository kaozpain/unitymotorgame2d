﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameState{Idle, playing, Ended, Ready};
public class GameController : MonoBehaviour {

	[Range (0f,0.20f)]
	public float parallaxSpeed=0.002f;
	public RawImage background;
	public GameObject uiIdle;
	public GameObject uiScore;
	public GameObject uimuerto;

	public GameState gameState = GameState.Idle;

	public GameObject player;
	public GameObject enemyGenerator;
	public GameObject cascoGenerator;

	public AudioClip musicPlayer;
	public AudioClip musicPlayer2;
	public AudioSource audio;

	public float scaleTime= 6f;
	public float scaleInc= .25f;

	public int points = 0;
	public int lifes = 1;
	public Text pointsText;
	public Text lifesText;
	public Text minusPoint;

	// Use this for initialization
	void Start () {
		uimuerto.SetActive (false);
		uiScore.SetActive (false);
		audio = GetComponent<AudioSource>();
		audio.clip = musicPlayer;
		audio.loop = true;
		audio.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		bool userAction = Input.GetKeyDown ("up") || Input.GetMouseButtonDown (0);
		if (gameState == GameState.Idle && userAction) {
			gameState = GameState.playing;
			uiIdle.SetActive (false);
			uiScore.SetActive (true);
			player.SendMessage ("UpdateState", "Driving");
			player.SendMessage ("DustPlay");
			enemyGenerator.SendMessage ("StartGenerator");
			cascoGenerator.SendMessage ("StartGenerator");
			audio.Stop ();
			audio.clip = musicPlayer2;
			audio.Play ();
			InvokeRepeating ("GameTimeScale", scaleTime, scaleTime);
		} else if (gameState == GameState.playing) {
			Parallax ();
		} else if (gameState == GameState.Ready) {
			if (userAction)
				RestarGame ();
		}
	}

	void Parallax(){
		float finalSpeed = parallaxSpeed + Time.deltaTime;	
		background.uvRect = new Rect (background.uvRect.x + parallaxSpeed, 0f, 1f, 1f);
	}

	public void RestarGame(){
		ResetTimeScale ();
		SceneManager.LoadScene ("HROMarketing");
	}

	void GameTimeScale(){
		Time.timeScale += scaleInc;
		Debug.Log ("Ritmo incrementado = " + Time.timeScale.ToString ());
	}

	public void ResetTimeScale(float newTimeScale=1f){
		CancelInvoke ("GameTimeScale");
		Time.timeScale = newTimeScale;
		Debug.Log ("Ritmo restablecido=" + Time.timeScale.ToString ());
	}

	public void IncreasePoints(){
		points++;
		pointsText.text = points.ToString ();
	}

	public void IncreaseLifes(){
		lifes++;
		lifesText.text=lifes.ToString ();
	}

	public void DecreaseLifes(){
		lifes--;
		if (lifes > 0) {
			lifesText.text = lifes.ToString ();
			player.SendMessage ("UpdateState", "playerHitted");
		}
		else {
			uimuerto.SetActive (true);
			lifesText.text="0";
			gameState = GameState.Ended;
		}
	}
}