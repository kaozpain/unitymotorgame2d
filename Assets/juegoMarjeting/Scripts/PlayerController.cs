﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public GameObject game;
	public GameObject enemyGenerator;
	public GameObject cascoGenerator;
	private Animator animator;
	public AudioClip dieClip;
	public ParticleSystem dust;
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
		bool gamePlaying = game.GetComponent<GameController> ().gameState == GameState.playing;
		if (gamePlaying && (Input.GetKeyDown ("up") || Input.GetMouseButtonDown (0))) {
			UpdateState ("PlayerJump");
		}
	}

	public void UpdateState(string state=null){
		if (state != null) {
			animator.Play (state);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "Enemy") {
			game.SendMessage ("DecreaseLifes");
			other.enabled = false;
			if (game.GetComponent<GameController> ().gameState == GameState.Ended) {
				UpdateState ("PlayerDie");
				enemyGenerator.SendMessage ("StopGenerator", true);
				cascoGenerator.SendMessage ("StopGenerator", true);
				game.SendMessage ("ResetTimeScale", 0.5f);
				game.GetComponent<AudioSource> ().Stop ();
				game.GetComponent<AudioSource> ().clip = dieClip;
				game.GetComponent<AudioSource> ().loop = false;
				game.GetComponent<AudioSource> ().Play ();
				DustStop ();
			}
		} else if (other.gameObject.tag == "Casco") {
			game.SendMessage ("IncreaseLifes");
		} else if (other.gameObject.tag == "Point") {
			game.SendMessage ("IncreasePoints");
		}
	}

	void GameReady(){
		game.GetComponent<GameController> ().gameState=GameState.Ready;
	}

	void DustPlay(){
		dust.Play ();
	}

	void DustStop(){
		dust.Stop ();
	}
}
